import { Injectable } from '@angular/core';
import { BookQuestion } from '../bookquestion';

@Injectable({
  providedIn: 'root'
})
export class QuizService {

  private questions: BookQuestion[] = [];
  private activeQuestion: BookQuestion;
  private isCorrect: boolean;
  private isSelected: boolean;
  private questionCounter: number;
  private optionCounter: number;
  private startTime: Date;
  private endTime: Date;
  private duration: number;
  private correctCounter: number;

  constructor() { }

  public addQuestions() {
    this.questions = [{
      book: 'The Stand',
      options: [
        'Anne Rice',
        'Stephen King',
        'Ernest Hemingway',
        'Jean Rowling'],
      correctOption: 1
    },
    {
    book: 'For Whom the Bell Tolls',
      options: [
        'Anne Rice',
        'Stephen King',
        'Ernest Hemingway',
        'Jean Rowling'],
      correctOption: 2
    },
    {
      book: 'Interview with the Vampire',
        options: [
        'Anne Rice',
        'Stephen King',
        'Ernest Hemingway',
        'Jean Rowling'],
        correctOption: 0
      },
      {
        book: 'Harry Potter and the deathly hallows',
          options: [
          'Anne Rice',
          'Stephen King',
          'Ernest Hemingway',
          'Jean Rowling'],
          correctOption: 3
        }
  ];
  }
  public setQuestion() {
    this.optionCounter = 0;
    this.isCorrect = false;
    this.isSelected = false;
    this.activeQuestion = this.questions[this.questionCounter];
    this.questionCounter++;
  }
  public initialize() {
    this.questionCounter = 0;
    this.startTime = new Date();
    this.setQuestion();
  }
  public getQuestions(): BookQuestion[] {
    return this.questions;
  }

  public getActiveQuestion(): BookQuestion {
    return this.activeQuestion;
  }

  public getQuoteOfActiveQuestion(): string {
    return this.activeQuestion.book;
  }

  public getOptionsOfActiveQuestion(): string[] {
    return this.activeQuestion.options;
  }

  public getIndexOfActiveQuestion(): number {
    return this.questionCounter;
  }

  public getNumberOfQuestions(): number {
    return this.questions.length;
  }

  public getNumberOfOptionsOfActiveQuestion(): number {
    return this.activeQuestion.options.length;
  }

  public getIndexOfOptionCounter(): number{
    return this.optionCounter;
  }

  public getCorrectOptionOfActiveQuestion(): string {
    return this.activeQuestion.options[this.activeQuestion.correctOption];
  }

  public setOptionSelected() {
    this.isSelected = true;
  }

  public isOptionSelected(): boolean {
    return this.isSelected;
  }

  public areAllOptionsUsed(): boolean {
    this.optionCounter++;
    return (this.optionCounter > this.activeQuestion.options.length) ? true : false;
  }

  public isCorrectOption(option: number): boolean {
    this.isCorrect = (option === this.activeQuestion.correctOption) ? true : false;
    return this.isCorrect;
  }



  public isAnswerCorrect(): boolean {
    return this.isCorrect;
  }

  public isFinished(): boolean {
    return (this.questionCounter === this.questions.length) ? true : false;
  }

  public getDuration(): number {
    this.endTime = new Date();
    this.duration = this.endTime.getTime() - this.startTime.getTime();
    return this.duration;
  }

}
