import { Component, OnInit } from '@angular/core';
import { QuizService } from '../quiz.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  constructor(public router: Router, private quizService: QuizService) {}


private feedback: string;
private duration: number;
private correctCounter: number;



ngOnInit() {
  this.quizService.addQuestions();
  this.quizService.initialize();
  this.feedback = '';
  this.correctCounter = 0;
}

private checkOption(option: number) {
  this.quizService.setOptionSelected();
  if (this.quizService.areAllOptionsUsed() ) {
    this.quizService.setQuestion();
  }
  else {
    if (this.quizService.isCorrectOption(option)) {
      this.correctCounter++;
      this.feedback =
      this.quizService.getCorrectOptionOfActiveQuestion() +
      ' is correct! Correct answers: ' + this.correctCounter;
    }
    else {
      this.feedback = ' Wrong answer. Correct answers: ' + this.correctCounter;

    }
  }
}

private continue() {
  if (this.quizService.isFinished()) {
    this.duration = this.quizService.getDuration();
    this.router.navigateByUrl('result/' + this.duration + this.correctCounter);
  }
  else {
    this.quizService.setQuestion();
  }
}

}
