import { Component, OnInit } from '@angular/core';
import { QuizService } from '../quiz.service';
import { Router, ActivatedRoute } from '@angular/router';
import { HomePage } from '../home/home.page';

@Component({
  selector: 'app-result',
  templateUrl: './result.page.html',
  styleUrls: ['./result.page.scss'],
})
export class ResultPage implements OnInit {

  constructor(public router: Router, public activatedRoute: ActivatedRoute, private quizService: QuizService) { }
    private duration: number;
    private durationSeconds: number;
    private correctCounter: number;



  ngOnInit() {
    this.duration = Number(this.activatedRoute.snapshot.paramMap.get('duration'));
    this.correctCounter = Number(this.activatedRoute.snapshot.paramMap.get('correctCounter'));
    this.durationSeconds = Math.round((this.duration) / 1000);
  }

  private goHome() {
    this.quizService.initialize();
    this.router.navigateByUrl('home');
  }

}
