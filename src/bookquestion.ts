export class BookQuestion {
    book: string;
    options: string[];
    correctOption: number;
}
